package org.ow.balancer.dto;

import lombok.Getter;
import lombok.Setter;
import org.ow.balancer.model.Team;

import java.util.List;

/**
 * Response information with resulting matchmaking.
 */
@Getter
@Setter
public class ResultDto {
    private double bestWorstDifference;
    private String mmrDistribution;
    private double averageMmr;
    private List<Team> teamList;
}

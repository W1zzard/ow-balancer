package org.ow.balancer.dto.json1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.ow.balancer.model.ClassType;

import java.util.List;

/**
 * DTO for parsing input json.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerDto {
    private String id;

    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("sr_by_class")
    private SrDto srByClass;

    private List<ClassType> classes;
    private boolean captain;
    private boolean weaponMaster;
}

package org.ow.balancer.controller.mapper;

import org.mapstruct.Mapper;
import org.ow.balancer.dto.BalanceRequestDto;
import org.ow.balancer.model.BalanceRequest;

/**
 * Map request DTO to balancer request model.
 */
@Mapper
public interface BalanceRequestMapper {
    BalanceRequest toBO(BalanceRequestDto source);
}

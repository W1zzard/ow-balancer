package org.ow.balancer.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Request with balance parameters.
 */
@Getter
@Setter
public class BalanceRequest {
    private int tankMmrWeight;

    private int dpsMmrWeight;

    private int supportMmrWeight;

    private int lowSkillMargin;

    private int lowSkillSameClassPenalty;
}

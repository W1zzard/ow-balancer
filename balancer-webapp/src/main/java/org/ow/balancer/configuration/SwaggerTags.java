package org.ow.balancer.configuration;

/**
 * Swagger tags.
 */
public final class SwaggerTags {

    public static final String ADMIN = "Admin";
    public static final String PLAYERS = "Players";
    public static final String BALANCER = "Balancer";

    private SwaggerTags() {
        throw new UnsupportedOperationException();
    }
}

import React, {useState} from "react";
import './styles.less'
import {Button, CardColumns, Col, Form} from 'react-bootstrap';
import {BalanceResponseModel} from "modules/core/models/BalanceResponseModel";
import {BalanceRequestModel} from "../../models/BalanceRequestModel";
import TeamComponent from "modules/core/components/team/TeamComponent";

const BalancerComponent: React.FC = () => {
    const [response, setResponse] = useState<BalanceResponseModel>({} as BalanceResponseModel);
    const [isLoading, setLoading] = useState(false);
    const [balanceRequest] = useState<BalanceRequestModel>({
        tankMmrWeight: 100,
        dpsMmrWeight: 100,
        supportMmrWeight: 100,
        lowSkillMargin: 0,
        lowSkillSameClassPenalty: 0,
    });

    function startBalance() {

        setLoading(true);
        requestBalance();
    }

    function requestBalance() {
        let query = Object.keys(balanceRequest)
            .map(k => encodeURIComponent(k) + '=' + encodeURIComponent((balanceRequest as any)[k]))
            .join('&');
        const requestOptions = {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        };
        fetch('/api/v1/balance/balanced?' + query, requestOptions)
            .then(response => response.json())
            .then(data => {
                setResponse(data);
                setLoading(false);
            });
    }

    return (
        <div className="mt-5">
            <Form>
                <Form.Row>
                    <Form.Group as={Col}>
                        <Form.Label className="mr-2" htmlFor="inlineFormCustomSelectPref">
                            Tank
                        </Form.Label>
                        <Form.Control
                            onChange={event => balanceRequest.tankMmrWeight = +event.target.value}
                            className="mb-1 mr-sm-1"
                            type="number"
                            id="tankInput"
                            defaultValue={balanceRequest.tankMmrWeight}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label className="mr-2" htmlFor="inlineFormCustomSelectPref">
                            DPS
                        </Form.Label>
                        <Form.Control
                            onChange={event => balanceRequest.dpsMmrWeight = +event.target.value}
                            className="mb-1 mr-sm-1"
                            type="number"
                            id="dpsInput"
                            defaultValue={balanceRequest.dpsMmrWeight}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label className="mr-2" htmlFor="inlineFormCustomSelectPref">
                            Support
                        </Form.Label>
                        <Form.Control
                            onChange={event => balanceRequest.supportMmrWeight = +event.target.value}
                            className="mb-1 mr-sm-1"
                            type="number"
                            id="supportInput"
                            defaultValue={balanceRequest.supportMmrWeight}
                        />
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col}>
                        <Form.Label className="mr-2" htmlFor="inlineFormCustomSelectPref">
                            low skill margin
                        </Form.Label>
                        <Form.Control
                            onChange={event => balanceRequest.lowSkillMargin = +event.target.value}
                            className="mb-1 mr-sm-1"
                            type="number"
                            id="supportInput"
                            defaultValue={balanceRequest.lowSkillMargin}
                        />
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label className="mr-2" htmlFor="inlineFormCustomSelectPref">
                            Low skill same class penalty
                        </Form.Label>
                        <Form.Control
                            onChange={event => balanceRequest.lowSkillSameClassPenalty = +event.target.value}
                            className="mb-1 mr-sm-1"
                            type="number"
                            id="supportInput"
                            defaultValue={balanceRequest.lowSkillSameClassPenalty}
                        />
                    </Form.Group>
                </Form.Row>
                <Button disabled={isLoading} className="mb-1"
                        onClick={startBalance}>balance</Button>
            </Form>
            <div>
                Best worst diff: {response.bestWorstDifference}, {response.mmrDistribution}
            </div>
            <CardColumns>
                {response.teamList?.map((team) => (
                    <TeamComponent team={team} averageMmr={response.averageMmr} key={team.name}/>
                ))}
            </CardColumns>
        </div>
    );
};

export default BalancerComponent;

package org.ow.balancer.matchmaking.step;

import org.ow.balancer.model.ClassType;
import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Matchmaking step for assigning weapon masters.
 */
public class MatchWeaponMasters implements MatchmakeStep {

    @Override
    public List<Player> fillTeams(final List<Team> teamList, final List<Player> playerList) {
        final List<Player> usedPlayerList = new ArrayList<>();

        teamList.sort(Comparator.comparingInt(Team::getMmrSum).reversed());
        playerList.sort(Comparator.comparingInt(Player::getCurrentClassMmr));
        final List<Team> teamsToFillList = new ArrayList<>(teamList);
        final List<Player> wmToFillList = new ArrayList<>(playerList);
        final Iterator<Player> wmIterator = wmToFillList.iterator();
        final Iterator<Team> teamIterator = teamsToFillList.iterator();
        while (wmIterator.hasNext()) {
            final Player wmPlayer = wmIterator.next();
            //WM class DPS or TANK should be assigned in priority to DPS captain
            if (ClassType.DPS == wmPlayer.getCurrentClass() || ClassType.TANK == wmPlayer.getCurrentClass()) {
                while (teamIterator.hasNext()) {
                    final Team teamToFill = teamIterator.next();
                    if (ClassType.DPS == teamToFill.getCaptain().getCurrentClass()) {
                        teamToFill.setWeaponMaster(wmPlayer);
                        usedPlayerList.add(wmPlayer);
                        wmIterator.remove();
                        teamIterator.remove();
                        break;
                    }
                }
            }
        }

        //all priority class WM and team filled on previous step and deleted from list of candidates
        for (int i = 0; i < wmToFillList.size(); i++) {
            final Player player = wmToFillList.get(i);
            usedPlayerList.add(player);
            teamsToFillList.get(i).setWeaponMaster(player);
        }

        return usedPlayerList;
    }
}

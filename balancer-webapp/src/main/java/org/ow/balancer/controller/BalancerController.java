package org.ow.balancer.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.ow.balancer.configuration.RestConfigurationUtils;
import org.ow.balancer.configuration.SwaggerTags;
import org.ow.balancer.controller.mapper.BalanceRequestMapper;
import org.ow.balancer.controller.mapper.ResponseMapper;
import org.ow.balancer.dto.BalanceRequestDto;
import org.ow.balancer.dto.ResultDto;
import org.ow.balancer.model.Team;
import org.ow.balancer.service.BalanceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller for actual balancer.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(RestConfigurationUtils.BASE_PATH_V1 + "/balance")
@Api(tags = SwaggerTags.BALANCER)
public class BalancerController {
    private final BalanceService balanceService;
    private final BalanceRequestMapper requestMapper;
    private final ResponseMapper responseMapper;

    @ApiOperation(value = "Try to balance all players in teams", notes = "Use to get actual balance")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @GetMapping("/balanced")
    public ResultDto getBalancedTeam(@Valid final BalanceRequestDto requestDto) {
        final List<Team> teamList = balanceService.getBalancedTeam(requestMapper.toBO(requestDto));
        return responseMapper.toResult(teamList);
    }
}

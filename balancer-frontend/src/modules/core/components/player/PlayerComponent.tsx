import React from "react";
import './styles.css'
import {PlayerModel} from "modules/core/models/PlayerModel";
import {ReactComponent as CaptainLogo} from './captain-svgrepo-com.svg';
import {ReactComponent as WMLogo} from './sword-svgrepo-com.svg';



export interface IPlayerComponent {
    player: PlayerModel;
}


const PlayerComponent: React.FC<IPlayerComponent> = ({player}) => {

    let classPriorityClass = 'player_class_priority_' + player.currentClassSkill.priority;

    const Captain = () => (
        <div style={{display: "inline", marginLeft: 20}}>
             <CaptainLogo style={{ height: 20, width: 20 }}/>
        </div>
    )

    const Weapon = () => (
        <div style={{display: "inline", marginLeft: 20}}>
            <WMLogo style={{ height: 20, width: 20 }}/>
        </div>
    )

    return (

        <div className={classPriorityClass}>
            <div>
                {player.displayName} - {player.currentClassSkill.classType} - {player.currentClassSkill.mmr}
                {player.playerType === 'CAPTAIN' ? Captain() : null}
                {player.playerType === 'WEAPONMASTER' ? Weapon() : null}
            </div>
        </div>
    );
};

export default PlayerComponent;

package org.ow.balancer.repository;

import org.mapstruct.Mapper;
import org.mapstruct.control.DeepClone;
import org.ow.balancer.model.Player;

/**
 * For deep copy of players.
 */
@Mapper(mappingControl = DeepClone.class)
public interface PlayerCopier {

    Player copy(Player source);
}

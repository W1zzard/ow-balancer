package org.ow.balancer.matchmaking.step;

import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Fill teams with 2 last players.
 */
public class FillFullTeam implements MatchmakeStep {
    private static final Logger LOGGER = LoggerFactory.getLogger(FillFullTeam.class);

    private final List<Player> usedPlayer = new ArrayList<>();

    private List<Player> playersSortedFromLowest;


    @Override
    public List<Player> fillTeams(final List<Team> teamList, final List<Player> playerList) {
        LOGGER.info("Start filling teams");
        final List<Team> teamsSortedFromBest = new ArrayList<>(teamList);
        teamsSortedFromBest.sort(Comparator.comparingInt(Team::getMmrSum).reversed());

        playersSortedFromLowest = new ArrayList<>(playerList);
        playersSortedFromLowest.sort(Comparator.comparingInt(Player::getCurrentClassMmr));

        for (final Team team : teamsSortedFromBest) {
            fillTeam(team);
        }
        return usedPlayer;
    }

    private void fillTeam(final Team team) {
        final ListIterator<Player> playerIterator = playersSortedFromLowest.listIterator(playersSortedFromLowest.size());
        //start from BEST
        while (playerIterator.hasPrevious()) {
            final Player player = playerIterator.previous();
            if (team.isFreeSlot(player.getCurrentClass())) {
                team.insertPlayer(player);
                usedPlayer.add(player);
                playerIterator.remove();
            }
        }
        //start from LOW
        for (Player player : playersSortedFromLowest) {
            if (team.isFreeSlot(player.getCurrentClass())) {
                team.insertPlayer(player);
                usedPlayer.add(player);
                playerIterator.remove();
            }
        }
    }
}

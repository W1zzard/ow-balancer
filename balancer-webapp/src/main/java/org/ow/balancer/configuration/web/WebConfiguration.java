package org.ow.balancer.configuration.web;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Controller;
import org.springframework.web.filter.ForwardedHeaderFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Spring MVC configuration.
 */
@RequiredArgsConstructor
@Configuration
@Controller
public class WebConfiguration implements WebMvcConfigurer {

    public static final String FRONTEND_REDIRECT = "frontend.redirect";

    /**
     * This filter is required for frontend development to handle redirects correctly.
     * While frontend development we have the following request route:
     * request -> local dev proxy -> reverse proxy -> server
     * So we need to handle X-Forwarded-* headers.
     */
    @Profile("dev")
    @Bean
    public FilterRegistrationBean forwardedHeaderFilter() {
        final FilterRegistrationBean<ForwardedHeaderFilter> filterRegistrationBean =
                new FilterRegistrationBean<>(new ForwardedHeaderFilter());
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return filterRegistrationBean;
    }

    @Profile("dev")
    @Bean
    public FilterRegistrationBean frontendForwardFilter() {
        final FilterRegistrationBean<FrontendForwardFilter> filterRegistrationBean =
                new FilterRegistrationBean<>(new FrontendForwardFilter());
        filterRegistrationBean.setOrder(Ordered.LOWEST_PRECEDENCE);
        return filterRegistrationBean;
    }
}

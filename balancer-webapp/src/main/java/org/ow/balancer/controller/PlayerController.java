package org.ow.balancer.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow.balancer.configuration.RestConfigurationUtils;
import org.ow.balancer.configuration.SwaggerTags;
import org.ow.balancer.dto.json1.InputDataDto;
import org.ow.balancer.dto.json1.Json1Mapper;
import org.ow.balancer.dto.json1.PlayerDto;
import org.ow.balancer.dto.json2.Json2Mapper;
import org.ow.balancer.model.*;
import org.ow.balancer.repository.Storage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller for players.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(RestConfigurationUtils.BASE_PATH_V1 + "/player")
@Api(tags = SwaggerTags.PLAYERS)
@Slf4j
public class PlayerController {
    private final Storage storage;
    private final Json1Mapper json1Mapper;
    private final Json2Mapper json2Mapper;

    @ApiOperation(value = "Batch Load players", notes = "Used to load many players via JSON")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @PostMapping("/batchLoad")
    public Integer batchLoadTest(@RequestBody final String jsonString) {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS, true);

        try {
            final InputDataDto inputDataDto = objectMapper.readValue(jsonString, InputDataDto.class);
            return storage.saveAllPlayers(convert(inputDataDto.getPlayers()));
        } catch (Exception exception) {
            log.error("Cant parse to 1 json type");
        }

        try {
            final org.ow.balancer.dto.json2.InputDataDto inputDataDto = objectMapper.readValue(jsonString, org.ow.balancer.dto.json2.InputDataDto.class);
            return storage.saveAllPlayers(convert2(inputDataDto));
        } catch (Exception exception) {
            log.error("Cant parse to 2 json type");
            throw new IllegalArgumentException();
        }
    }

    @ApiOperation(value = "Batch Load players", notes = "Used to load many players via JSON")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @PostMapping()
    public void addPlayer(@RequestBody final PlayerDto playerDto) {
        storage.savePlayer(json1Mapper.toPlayer(playerDto));
    }

    private List<Player> convert(final List<PlayerDto> playerDtoList) {
        return playerDtoList.stream()
                .map(json1Mapper::toPlayer)
                .collect(Collectors.toList());
    }

    private List<Player> convert2(final org.ow.balancer.dto.json2.InputDataDto inputDataDto) {
        return inputDataDto.getPlayers().values().stream()
                .map(json2Mapper::toPlayer)
                .collect(Collectors.toList());
    }

}

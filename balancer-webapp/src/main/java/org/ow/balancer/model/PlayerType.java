package org.ow.balancer.model;

/**
 * Player type enum.
 */
public enum PlayerType {
    CAPTAIN, WEAPONMASTER, COMMON
}

package org.ow.balancer.matchmaking.step;

import org.ow.balancer.matchmaking.util.BalanceUtil;
import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Balance players with their main classes between teams only.
 */
public class BalanceBetweenTeamMainClass implements MatchmakeStep {
    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceBetweenTeamMainClass.class);
    private final Map<Player, List<Player>> playerToPlayerHistoryMap = new HashMap<>();

    private List<Team> teamsSortedFromBest;


    @Override
    public List<Player> fillTeams(final List<Team> teamList, final List<Player> playerList) {
        teamsSortedFromBest = new ArrayList<>(teamList);

        boolean lastBalance = true;

        while (lastBalance) {
            teamsSortedFromBest.sort(Comparator.comparingInt(Team::getMmrSum).reversed());
            final double averageFullTeamMmr = teamsSortedFromBest.stream().mapToDouble(Team::getAverageMmr).average().getAsDouble();
            final Team bestTeam = teamsSortedFromBest.get(0);
            //if best and worst team dif from average lower than thresholds then balance not needed
            if (BalanceUtil.isBalancesEnough(teamsSortedFromBest)) {
                LOGGER.info("Balance not needed");
                return null;
            }
            LOGGER.info("Try to balance team, BEST team: {}, averageMMR: {}", bestTeam, averageFullTeamMmr);
            lastBalance = balanceBestTeam(bestTeam);
        }

        return null;
    }


    private boolean balanceBestTeam(final Team bestTeam) {
        final ListIterator<Team> backwardTeamIter = teamsSortedFromBest.listIterator(teamsSortedFromBest.size());
        while (backwardTeamIter.hasPrevious()) {
            final Team lowTeam = backwardTeamIter.previous();
            if (swapExactClass(bestTeam, lowTeam)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Try to find player to swap WITHOUT change classes.
     *
     * @param teamHigh high rated team
     * @param teamLow  low rated team
     * @return <b>true</b> if high rated team rate was decreased
     */
    private boolean swapExactClass(final Team teamHigh, final Team teamLow) {
        for (final Player playerFromHigh : teamHigh.getSwapablePlayers()) {
            for (final Player playerFromLow : teamLow.getSwapablePlayers()) {
                //if class check OK AND high player have more MMR, AND we dont switch same players in loop
                if (playerFromHigh.getCurrentClass() == playerFromLow.getCurrentClass()
                        && playerFromHigh.getCurrentClassMmr() > playerFromLow.getCurrentClassMmr()
                        && (playerToPlayerHistoryMap.get(playerFromHigh) == null || !playerToPlayerHistoryMap.get(playerFromHigh).contains(playerFromLow))) {
                    teamHigh.removePlayer(playerFromHigh);
                    teamLow.removePlayer(playerFromLow);
                    teamHigh.insertPlayer(playerFromLow);
                    teamLow.insertPlayer(playerFromHigh);
                    playerToPlayerHistoryMap.compute(playerFromHigh, (player, playerList) -> {
                        if (playerList == null) {
                            final ArrayList<Player> players = new ArrayList<>();
                            players.add(playerFromLow);
                            return players;
                        } else {
                            playerList.add(playerFromLow);
                            return playerList;
                        }
                    });
                    LOGGER.info("Found player to switch, HIGH team plyer: {}, low team player: {}", playerFromHigh, playerFromLow);
                    return true;
                }
            }
        }
        return false;
    }

}

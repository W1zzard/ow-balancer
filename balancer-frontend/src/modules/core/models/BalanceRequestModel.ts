export interface BalanceRequestModel {
    tankMmrWeight: number;
    dpsMmrWeight: number;
    supportMmrWeight: number;
    lowSkillMargin: number;
    lowSkillSameClassPenalty: number;
}

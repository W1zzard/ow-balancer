import React from "react";
import './styles.css'
import { Card } from 'react-bootstrap';
import {TeamModel} from "modules/core/models/TeamModel";
import PlayerComponent from "modules/core/components/player/PlayerComponent";


export interface ITeamComponent {
    team: TeamModel;
    averageMmr: number;
}

const TeamComponent: React.FC<ITeamComponent> = ({team, averageMmr}) => {

    function getDiffString() {
        let difference = team.averageMmr - averageMmr;
        if (difference > 0) {
            return 'avg +' + difference.toFixed(2);
        } else {
            return 'avg ' + difference.toFixed(2);
        }
    }

    return (
        <Card style={{ width: '22rem' }}>
            <Card.Header><h4>Team: {team.name}</h4></Card.Header>
            <Card.Body>MMR: {team.averageMmr.toFixed(2)} {getDiffString()}</Card.Body>
            <div className="team_content">
                {team.playerList.map((plr) => (
                    <PlayerComponent player={plr} key={plr.uniqueName}/>
                ))}
            </div>
        </Card>
    );
};

export default TeamComponent;

package org.ow.balancer.repository;

import lombok.RequiredArgsConstructor;
import org.ow.balancer.model.Player;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Some storage mock, with no SQL or persistence layer.
 */
@RequiredArgsConstructor
@Component
public class Storage {
    private static final Set<Player> PLAYER_SET = new HashSet<>();

    private final PlayerCopier playerCopier;

    public void clear() {
        PLAYER_SET.clear();
    }

    public void savePlayer(final Player player) {
        PLAYER_SET.add(player);
    }

    public int saveAllPlayers(final List<Player> playerList) {
        final int sizeBefore = PLAYER_SET.size();
        PLAYER_SET.addAll(playerList);
        return PLAYER_SET.size() - sizeBefore;
    }

    public List<Player> getAllPlayers() {
        return PLAYER_SET.stream()
                .map(playerCopier::copy)
                .collect(Collectors.toList());
    }
}

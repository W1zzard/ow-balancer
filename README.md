# Balancer

Project to balance OW teams by rating

## Development

### Configure PostgreSQL
Run the following commands from the Linux shell:
```
sudo -u postgres psql
> create database balancer;
> drop role if exists ow;
> create role ow with login encrypted password 'ow';
> grant all privileges on database 'ow_balancer' to ow;
> \q
```
#### Usage


sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
package org.ow.balancer.configuration;

import com.google.common.base.Predicate;
import springfox.documentation.service.Tag;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Configuration utilities.
 */
public final class RestConfigurationUtils {

    public static final String WILDCARD = ".*";
    public static final String BASE_PATH_V1 = "/api/v1";

    private RestConfigurationUtils() {
        throw new UnsupportedOperationException();
    }

    public static Predicate<String> getApiPaths(final String basePath) {
        return regex(basePath + WILDCARD);
    }

    public static Tag tag(final String name) {
        return new Tag(name, "");
    }
}

package org.ow.balancer.matchmaking.step;


import org.ow.balancer.matchmaking.condition.PointsCondition;
import org.ow.balancer.matchmaking.util.BalanceUtil;
import org.ow.balancer.model.ClassSkill;
import org.ow.balancer.model.ClassType;
import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Balance full team with external players.
 */
public class BalanceWithExternalPlayersMainClass implements MatchmakeStep {
    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceWithExternalPlayersMainClass.class);
    //used for double roundnes effect to remove.
    private static final double WIN_POINTS_MARGIN = 0.1;
    private final Map<Player, List<Player>> playerToPlayerHistoryMap = new HashMap<>();
    private final List<PointsCondition> pointsConditionList = new ArrayList<>();

    private List<Team> teamsSortedFromWorst;
    private List<Player> externalPlayerFromBest;

    public void addPointsCondition(final PointsCondition pointsCondition) {
        pointsConditionList.add(pointsCondition);
    }

    @Override
    public List<Player> fillTeams(final List<Team> teamList, final List<Player> playerList) {
        teamsSortedFromWorst = new ArrayList<>(teamList);
        externalPlayerFromBest = new ArrayList<>(playerList);

        boolean lastBalance = true;

        while (lastBalance) {
            teamsSortedFromWorst.sort(Comparator.comparingInt(Team::getMmrSum));
            externalPlayerFromBest.sort(Comparator.comparingInt(Player::getCurrentClassMmr));

            final double averageFullTeamMmr = teamsSortedFromWorst.stream().mapToDouble(Team::getAverageMmr).average().getAsDouble();
            final Team worstTeam = teamsSortedFromWorst.get(0);
            //if best and worst team dif from average lower than thresholds then balance not needed
            if (BalanceUtil.isBalancesEnough(teamsSortedFromWorst)) {
                LOGGER.info("Balance not needed");
                return null;
            }
            LOGGER.info("Try to balance team, worst team: {}, averageMMR: {}", worstTeam, averageFullTeamMmr);
            lastBalance = balanceWorstTeam(worstTeam, averageFullTeamMmr);
            if (!lastBalance) {
                //try best from other end
                lastBalance = balanceWorstTeam(teamsSortedFromWorst.get(teamsSortedFromWorst.size() - 1), averageFullTeamMmr);
            }
        }

        return externalPlayerFromBest;
    }


    private boolean balanceWorstTeam(final Team worstTeam, final double averageMMR) {
        final List<Player> allPossibleCandidatePlayerForTeam = getAllPossibleCandidatePlayerForTeam(worstTeam);
        //allPossibleCandidatePlayerForTeam.sort(Comparator.comparingInt(Player::getCurrentClassMmr).reversed());
        for (Player playerCandidate : allPossibleCandidatePlayerForTeam) {
            if (trySwapPlayer(worstTeam, playerCandidate, averageMMR)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Try to swap best player into lower team.
     *
     * @param teamLow    low rated team
     * @param bestPlayer best external player
     * @return <b>true</b> if low rated team rate was increased
     */
    private boolean trySwapPlayer(final Team teamLow, final Player bestPlayer, final double averageMMR) {
        for (final Player playerFromLow : teamLow.getSwapablePlayers()) {
            if (canSwap(playerFromLow, bestPlayer, averageMMR)) {
                final ClassType lowPlayerClass = playerFromLow.getCurrentClass();
                final ClassType bestPlayerClass = bestPlayer.getCurrentClass();

                final Team bestPlayerTeam = bestPlayer.getTeam();
                if (bestPlayerTeam != null) {
                    bestPlayerTeam.removePlayer(bestPlayer);
                    teamLow.removePlayer(playerFromLow);
                    bestPlayer.switchClassTo(lowPlayerClass);
                    playerFromLow.switchClassTo(bestPlayerClass);
                    teamLow.insertPlayer(bestPlayer);
                    bestPlayerTeam.insertPlayer(playerFromLow);
                } else {
                    bestPlayer.switchClassTo(lowPlayerClass);
                    teamLow.removePlayer(playerFromLow);
                    teamLow.insertPlayer(bestPlayer);
                    externalPlayerFromBest.remove(bestPlayer);
                    externalPlayerFromBest.add(playerFromLow);
                }
                playerToPlayerHistoryMap.compute(playerFromLow, (player, playerList) -> {
                    if (playerList == null) {
                        final ArrayList<Player> players = new ArrayList<>();
                        players.add(bestPlayer);
                        return players;
                    } else {
                        playerList.add(bestPlayer);
                        return playerList;
                    }
                });
                LOGGER.info("Found player to switch, low team plyer: {}, HIGH external player: {}", playerFromLow, bestPlayer);
                return true;
            }
        }
        return false;
    }

    private boolean canSwap(final Player playerInTeam, final Player playerCandidate, final double averageMMR) {
        Double winPointForward = calculateWinPoint(playerInTeam, playerCandidate, averageMMR);
        Double winPointBackward = calculateWinPoint(playerCandidate, playerInTeam, averageMMR);
        //non negative means subclass allowed, win points is result of swap reduce spread.
        final boolean subclassAllowed = !winPointForward.isInfinite() && !winPointBackward.isInfinite();
        if (subclassAllowed) {
            winPointForward = calculateConditionPoints(playerInTeam, playerCandidate, winPointForward);
            winPointBackward = calculateConditionPoints(playerCandidate, playerInTeam, winPointBackward);
        }
        final boolean overallWinpointsPositive = winPointBackward + winPointForward > WIN_POINTS_MARGIN;
        final boolean notAlreadySwitched = playerToPlayerHistoryMap.get(playerInTeam) == null
                || !playerToPlayerHistoryMap.get(playerInTeam).contains(playerCandidate);
        final boolean canSwap = subclassAllowed && overallWinpointsPositive && notAlreadySwitched;
        if (canSwap) {
            LOGGER.info("Can swap, winPointsForward: {}, bacward: {}", winPointForward, winPointBackward);
        }
        return canSwap;
    }

    private Double calculateWinPoint(final Player player, final Player playerCandidate, final double averageMMR) {
        //if player not in team, he could be swapped no class checking
        if (player.getTeam() == null) {
            return (double) 0;
        }
        //if player in team the one who swap should be able to swap
        for (ClassSkill classSkillCandidate : playerCandidate.getAvailableClasses()) {
            //candidate have proper class
            if (player.getCurrentClass() == classSkillCandidate.getClassType()) {
                //calculate MMR dif point winning
                return BalanceUtil.closerToAveragePoints(player, classSkillCandidate.getMmr(), averageMMR);
            }
        }
        //if player candidate cant swap with player return INF negative
        return Double.NEGATIVE_INFINITY;
    }

    private List<Player> getAllPossibleCandidatePlayerForTeam(final Team balancedTeam) {
        final List<Player> result = teamsSortedFromWorst.stream()
                .filter(team -> !balancedTeam.equals(team))
                .flatMap(team -> team.getSwapablePlayers().stream())
                .collect(Collectors.toList());
        result.addAll(externalPlayerFromBest);

        return result;
    }

    private Double calculateConditionPoints(final Player playerInTeam, final Player playerCandidate, final Double winPoint) {
        Double result = winPoint;
        for (PointsCondition condition : pointsConditionList) {
            result = result + condition.apply(playerInTeam, playerCandidate);
        }
        return result;
    }
}

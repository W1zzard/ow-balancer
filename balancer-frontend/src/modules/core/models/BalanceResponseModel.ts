import {TeamModel} from "modules/core/models/TeamModel";


export interface BalanceResponseModel {
    bestWorstDifference : number;
    mmrDistribution: string;
    averageMmr : number;
    teamList : TeamModel[];
}

package org.ow.balancer.dto.json2;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;


/**
 * PlayerDto.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerDto {
    private IdentityDto identity;
    private StatsDto stats;
}

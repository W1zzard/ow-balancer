package org.ow.balancer.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Weight coefficent for balancing in percents.
 */
@Getter
@Setter
public class BalanceRequestDto {

    @Min(value = 1, message = "Should be between 1-200")
    @Max(value = 500, message = "Should be between 1-200")
    private int tankMmrWeight;

    @Min(value = 1, message = "Should be between 1-200")
    @Max(value = 500, message = "Should be between 1-200")
    private int dpsMmrWeight;

    @Min(value = 1, message = "Should be between 1-200")
    @Max(value = 500, message = "Should be between 1-200")
    private int supportMmrWeight;

    @Min(value = 0, message = "Should be between 0-5000")
    @Max(value = 5000, message = "Should be between 0-5000")
    private int lowSkillMargin;

    @Min(value = 0, message = "Should be between 0-500")
    @Max(value = 500, message = "Should be between 0-500")
    private int lowSkillSameClassPenalty;
}

package org.ow.balancer.dto;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.ow.balancer.dto.json1.InputDataDto;
import org.ow.balancer.dto.json1.PlayerDto;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
public class InputDataDtoTest extends TestCase {
    private static final String BASE_PATH = "classpath:";

    @Test
    public void testJsonParsing() throws IOException {
        final ClassLoader classLoader = getClass().getClassLoader();
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);

        final Resource resource = resolver.getResource(BASE_PATH + "example.json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS, true);

        //convert json string to object
        InputDataDto inputDataDto = objectMapper.readValue(resource.getInputStream(), InputDataDto.class);

        Assert.assertNotNull(inputDataDto);
        Assert.assertEquals("tournament", inputDataDto.getFormatType());
        Assert.assertEquals(168, inputDataDto.getPlayers().size());
        final List<PlayerDto> captainsList = inputDataDto.getPlayers().stream()
                .filter(PlayerDto::isCaptain)
                .collect(Collectors.toList());
        Assert.assertFalse(captainsList.isEmpty());
        Assert.assertEquals(23, captainsList.size());
    }
}
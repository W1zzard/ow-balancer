package org.ow.balancer.dto.json2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * identity Dto.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IdentityDto {
    private String name;
    private Boolean isCaptain;
    private Boolean isSquire;

}

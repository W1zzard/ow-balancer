package org.ow.balancer.dto.json1;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO for parsing input json.
 */
@Getter
@Setter
public class SrDto {
    private int tank;
    private int dps;
    private int support;
}

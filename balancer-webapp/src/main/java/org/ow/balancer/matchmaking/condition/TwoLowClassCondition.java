package org.ow.balancer.matchmaking.condition;

import lombok.AllArgsConstructor;
import org.ow.balancer.model.ClassSkill;
import org.ow.balancer.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition which affects points if player switch will result in two lower tier same classes.
 */
@AllArgsConstructor
public class TwoLowClassCondition implements PointsCondition {
    private static final Logger LOGGER = LoggerFactory.getLogger(TwoLowClassCondition.class);
    private final Double lowClassMargin;
    private final Double penaltyScore;

    @Override
    public Double apply(final Player playerInTeam, final Player playerCandidate) {
        final Player secondPlayerOnRole = playerInTeam.getTeam().getPlayerList().stream()
                .filter(player -> player.getCurrentClass() == playerInTeam.getCurrentClass())
                .filter(player -> !player.equals(playerInTeam))
                .findFirst()
                .orElse(null);

        final ClassSkill playerCandidateProperClassSkill = playerCandidate.getAvailableClasses().stream()
                .filter(classSkill -> classSkill.getClassType() == playerInTeam.getCurrentClass())
                .findFirst()
                .orElse(null);

        if (secondPlayerOnRole != null && playerCandidateProperClassSkill != null
                && secondPlayerOnRole.getCurrentClassMmr() < lowClassMargin && playerCandidateProperClassSkill.getMmr() < lowClassMargin) {
            LOGGER.info("Condition found, use penalty score: {}", penaltyScore);
            return -penaltyScore;
        }
        return 0d;
    }
}

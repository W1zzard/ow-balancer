package org.ow.balancer;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

// CHECKSTYLE.OFF: UncommentedMain|HideUtilityClassConstructor

/**
 * Spring boot application runner.
 */
@RequiredArgsConstructor
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
@EnableAsync(proxyTargetClass = true)
@EnableScheduling
@EnableCaching
public class Application {


    @Bean
    public Executor replicationTaskExecutor() {
        return Executors.newSingleThreadExecutor();
    }


    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

// CHECKSTYLE.ON: UncommentedMain|HideUtilityClassConstructor

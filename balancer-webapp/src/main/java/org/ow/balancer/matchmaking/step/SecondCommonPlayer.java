package org.ow.balancer.matchmaking.step;

import org.ow.balancer.model.ClassType;
import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Selecting second common player.
 */
public class SecondCommonPlayer implements MatchmakeStep {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecondCommonPlayer.class);

    private final List<Team> vacantTeamList = new ArrayList<>();
    private final List<Player> usedPlayerList = new ArrayList<>();

    private Team firstBestTeam;
    private Team secondBestTeam;
    private Team thirdBestTeam;

    @Override
    public List<Player> fillTeams(final List<Team> teamList, final List<Player> playerList) {
        vacantTeamList.addAll(teamList);

        //start finding tema for player from most skillful player into less skilled team
        playerList.sort(Comparator.comparingInt(Player::getCurrentClassMmr).reversed());
        teamList.sort(Comparator.comparingInt(Team::getMmrSum));
        for (final Player playerCandidate : playerList) {
            findTeamForPlayer(playerCandidate);
            if (vacantTeamList.isEmpty()) {
                break;
            }
        }
        //players could be more than team, finish step when player added to all teams
        return usedPlayerList;
    }

    private boolean findTeamForPlayer(final Player playerCandidate) {
        firstBestTeam = null;
        secondBestTeam = null;
        thirdBestTeam = null;
        for (final Team team : vacantTeamList) {
            final ClassType candidateClass = playerCandidate.getCurrentClass();
            if (isBestSuitedTeamFound(team, candidateClass)) {
                break;
            }
        }

        if (firstBestTeam != null) {
            insertPlayerToTeam(playerCandidate, firstBestTeam);
            return true;
            // if not found best team then try second best
        } else if (secondBestTeam != null) {
            insertPlayerToTeam(playerCandidate, secondBestTeam);
            return true;
            //if no second best try at least empty slot
        } else if (thirdBestTeam != null) {
            insertPlayerToTeam(playerCandidate, thirdBestTeam);
            return true;
        } else {
            LOGGER.warn("Cant find even open slot for player: {}", playerCandidate);
            return false;
            //throw new UnableToProceedBalancingException("Cant find even free slot for fourth player");
        }
    }

    private boolean isBestSuitedTeamFound(final Team team, final ClassType candidateClass) {
        //check 3rd best team at least open slot
        if (team.isFreeSlot(candidateClass)) {
            if (thirdBestTeam == null) {
                thirdBestTeam = team;
            }

            //check second best option
            if (team.getCaptain().getCurrentClass() != candidateClass) {
                if (secondBestTeam == null) {
                    secondBestTeam = team;
                }

                //check first best option
                if (team.getThirdPlayer().getCurrentClass() != candidateClass) {
                    firstBestTeam = team;
                    return true;
                }
            }
        }
        return false;
    }

    private void insertPlayerToTeam(final Player playerCandidate, final Team foundedTeam) {
        foundedTeam.setFourthPlayer(playerCandidate);
        vacantTeamList.remove(foundedTeam);
        usedPlayerList.add(playerCandidate);
    }

}

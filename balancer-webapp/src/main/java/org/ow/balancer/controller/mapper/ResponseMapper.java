package org.ow.balancer.controller.mapper;

import org.mapstruct.Mapper;
import org.ow.balancer.dto.ResultDto;
import org.ow.balancer.model.Team;

import java.util.List;

/**
 * Map list of balanced team into response DTO.
 */
@Mapper
public class ResponseMapper {
    private static final int MAXIMUM_MMR = 10000;

    public ResultDto toResult(final List<Team> teamList) {
        final ResultDto resultDto = new ResultDto();
        resultDto.setBestWorstDifference(0);

        double lowestTeamMmr = MAXIMUM_MMR;
        double highestTeamMmr = 0;
        double averageMmr = 0;
        for (Team team : teamList) {
            if (team.getAverageMmr() < lowestTeamMmr) {
                lowestTeamMmr = team.getAverageMmr();
            }
            if (team.getAverageMmr() > highestTeamMmr) {
                highestTeamMmr = team.getAverageMmr();
            }
            averageMmr += team.getAverageMmr();
        }
        resultDto.setBestWorstDifference(highestTeamMmr - lowestTeamMmr);
        averageMmr = averageMmr / teamList.size();
        resultDto.setMmrDistribution(teamList.size() + " teams with average MMR: " + averageMmr + "+-" + Math.max(highestTeamMmr - averageMmr,
                averageMmr - lowestTeamMmr));
        resultDto.setTeamList(teamList);
        resultDto.setAverageMmr(averageMmr);
        return resultDto;
    }
}

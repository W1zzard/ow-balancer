package org.ow.balancer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Player model.
 */
@Setter
@Getter
@ToString
@EqualsAndHashCode(of = {"uniqueName"})
public class Player {
    private String uniqueName;
    private String displayName;
    private PlayerType playerType;

    @JsonIgnore
    private int currentClassMmr;
    @JsonIgnore
    private ClassType currentClass;
    private ClassSkill currentClassSkill;

    //should be sorted from max
    @JsonIgnore
    private List<ClassSkill> availableClasses = new ArrayList<>();

    @JsonIgnore
    private Team team;

    public boolean isFlex() {
        return availableClasses.size() > 1;
    }

    public boolean canSwitchClassTo(final ClassType toClass) {
        return availableClasses.stream().map(ClassSkill::getClassType).anyMatch(classType -> classType == toClass);
    }

    public int getSubclassMmr(final ClassType subClass) {
        return availableClasses.stream().filter(classSkill -> classSkill.getClassType() == subClass).findAny().map(ClassSkill::getMmr).orElse(0);
    }

    public void switchClassTo(final ClassType toClass) {
        final ClassSkill switchedToClassSkill = availableClasses.stream()
                .filter(classSkill -> classSkill.getClassType() == toClass)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Cant switch to this class: " + toClass));
        currentClass = switchedToClassSkill.getClassType();
        currentClassMmr = switchedToClassSkill.getMmr();
        currentClassSkill = switchedToClassSkill;
    }
}

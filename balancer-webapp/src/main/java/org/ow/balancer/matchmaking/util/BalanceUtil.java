package org.ow.balancer.matchmaking.util;

import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Utility class for balancing support methods.
 */
public final class BalanceUtil {
    public static final double BALANCE_THRESHOLDS = 10;

    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceUtil.class);

    private BalanceUtil() {
    }

    /**
     * SORTED team list required.
     * @param sortedTeamList
     * @return true if not need further balance
     */
    public static boolean isBalancesEnough(final List<Team> sortedTeamList) {
        final double averageFullTeamMmr = sortedTeamList.stream().mapToDouble(Team::getAverageMmr).average().getAsDouble();
        final double differenceLeftBorder = Math.abs(sortedTeamList.get(0).getAverageMmr() - averageFullTeamMmr);
        final double differenceRightBorder = Math.abs(sortedTeamList.get(sortedTeamList.size() - 1).getAverageMmr() - averageFullTeamMmr);
        LOGGER.info("Difference left: {}, right: {}", differenceLeftBorder, differenceRightBorder);
        return differenceLeftBorder < BALANCE_THRESHOLDS && differenceRightBorder < BALANCE_THRESHOLDS;
    }

    /**
     * Calculate number of points which bring team closer to average if swap.
     *
     * @param inTeamPlayer player in team
     * @param averageMMR   average mmr for all teams
     * @param candidateMmr candidate mmr
     * @return positive nomber of points if result become closer, negative if worse
     */

    public static double closerToAveragePoints(final Player inTeamPlayer, final double candidateMmr, final double averageMMR) {
        final Team team = inTeamPlayer.getTeam();
        final double difBeforeSwap = Math.abs(averageMMR - team.getAverageMmr());
        final double newTeamMmr =
                (team.getAverageMmr() * team.getPlayerList().size() - inTeamPlayer.getCurrentClassMmr() + candidateMmr) / team.getPlayerList().size();
        final double difAfterSwap = Math.abs(averageMMR - newTeamMmr);
        return difBeforeSwap - difAfterSwap;
    }
}

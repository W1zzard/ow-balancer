package org.ow.balancer.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * model to store class and skill for this class.
 */
@Getter
@Setter
@ToString
public class ClassSkill {
    private final ClassType classType;
    private int mmr;
    //main class is 1, secondary 2, third 3
    private int priority;

    public ClassSkill(final ClassType classType, final int mmr) {
        this.classType = classType;
        this.mmr = mmr;
    }
}

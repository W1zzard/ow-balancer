package org.ow.balancer.dto.json2;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * inputDataDto.
 */
@Getter
@Setter
public class InputDataDto {
    private String format;
    private Map<String, PlayerDto> players;
}

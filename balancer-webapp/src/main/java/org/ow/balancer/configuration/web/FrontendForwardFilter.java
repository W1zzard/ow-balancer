package org.ow.balancer.configuration.web;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * {@link org.springframework.web.filter.ForwardedHeaderFilter} does not work with forward URLs.
 * This filter is intended to fix this for frontend redirects.
 */
public class FrontendForwardFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(final HttpServletRequest request,
                                    final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {

        filterChain.doFilter(new HttpServletRequestWrapper(request) {
            @Override
            public String getRequestURI() {
                if (Boolean.TRUE.equals(getAttribute(WebConfiguration.FRONTEND_REDIRECT))) {
                    return "/";
                }
                return super.getRequestURI();
            }
        }, response);
    }
}

package org.ow.balancer.matchmaking;

import org.ow.balancer.model.ClassType;
import org.ow.balancer.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * check classes slots and amount of players of this class, trying to use second classes to fill all slots.
 */
public class ClassesRebalance {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassesRebalance.class);
    private static final int INFINITE_MMR = 10000;

    private final Map<ClassType, Integer> vacantClassNumber = new HashMap<>();
    private final Map<ClassType, List<Player>> classPlaces = new HashMap<>();

    private final List<Player> oneTrickPlayerList = new ArrayList<>();
    private final List<Player> flexPlayerList = new ArrayList<>();

    private final int overallSlotsOfOneClass;

    public ClassesRebalance(final List<Player> captainList, final List<Player> playerList) {
        overallSlotsOfOneClass = captainList.size() * 2;
        fillVacantNumber(captainList);
        fillFlexOnetrick(playerList);
        //flexPlayerList.sort(Comparator.comparingInt(Player::getCurrentClassMmr));
        LOGGER.info("Captains/Teams number: {}", captainList.size());
        LOGGER.info("Vacant slots: {}", vacantClassNumber);
        LOGGER.info("Onetrick players: {}", oneTrickPlayerList.size());
        LOGGER.info("Flex players: {}", flexPlayerList.size());

        //first count onetrick because we do not have options to test their class
        oneTrickPlayerList.forEach(player -> {
            vacantClassNumber.computeIfPresent(player.getCurrentClass(), (classType, integer) -> {
                if (integer > 0) {
                    return integer - 1;
                }
                return 0;
            });
        });

        LOGGER.info("After onetrick substract from available classes: {}", vacantClassNumber);

        vacantClassNumber.forEach((classType, integer) -> {
            classPlaces.put(classType, new ArrayList<>());
        });

        flexPlayerList.forEach(player -> classPlaces.get(player.getCurrentClass()).add(player));
    }

    public void balanceClasses() {
        while (isSwitchingNeeded()) {
            LOGGER.info("Start switching");
            //sort each class in map from lowest player to best
            classPlaces.values().forEach(players -> players.sort(Comparator.comparingInt(Player::getCurrentClassMmr)));

            final ClassType classTypeSwitch = findMostOverwhelmedClass();
            final ClassType classTypeSwitchTo = findMostVacantClass();
            LOGGER.info("Too MANY of classes: {}:{}", classTypeSwitch, classPlaces.get(classTypeSwitch).size());
            LOGGER.info("Too low of classes: {}:{}", classTypeSwitchTo, classPlaces.get(classTypeSwitchTo).size());

            //try to find player with less class dif to switch
            Player playerToSwitch = findPlayerToSwitchClassOneStep(classTypeSwitch, classTypeSwitchTo);

            //if cant find player to switch to needed role, try two step switch
            if (playerToSwitch != null) {
                LOGGER.info("find one player to switch: {}", playerToSwitch);
                playerToSwitch.switchClassTo(classTypeSwitchTo);
                classPlaces.get(classTypeSwitch).remove(playerToSwitch);
                classPlaces.get(classTypeSwitchTo).add(playerToSwitch);
            } else {
                final List<ClassType> classTypes = Arrays.asList(ClassType.values().clone());
                classTypes.removeAll(Arrays.asList(classTypeSwitch, classTypeSwitchTo));
                final ClassType middleClass = classTypes.get(0);
                LOGGER.info("Player to switch not found try to use middle class: {}", middleClass);

                playerToSwitch = findPlayerToSwitchClassOneStep(classTypeSwitch, middleClass);
                final Player playerHelper = findPlayerToSwitchClassOneStep(middleClass, classTypeSwitchTo);
                if (playerToSwitch != null && playerHelper != null) {
                    playerToSwitch.switchClassTo(middleClass);
                    classPlaces.get(classTypeSwitch).remove(playerToSwitch);
                    classPlaces.get(middleClass).add(playerToSwitch);

                    playerHelper.switchClassTo(classTypeSwitchTo);
                    classPlaces.get(middleClass).remove(playerHelper);
                    classPlaces.get(classTypeSwitchTo).add(playerHelper);
                } else {
                    LOGGER.info("Cant switch anything stop process");
                    //cant switch anyone.
                    break;
                }
            }
        }
    }

    //find player to switch by lowest diff
    private Player findPlayerToSwitchClassOneStep(final ClassType from, final ClassType to) {
        int mmrDiff = INFINITE_MMR;
        Player candidate = null;

        for (final Player player : classPlaces.get(from)) {
            //todo this rule could be update Anak said that better to change lowest player it will impact less
            if (player.canSwitchClassTo(to) && Math.abs(player.getCurrentClassMmr() - player.getSubclassMmr(to)) < mmrDiff) {
                candidate = player;
                mmrDiff = player.getCurrentClassMmr() - player.getSubclassMmr(to);
            }
        }

        return candidate;
    }

    private ClassType findMostOverwhelmedClass() {
        int difference = 0;
        ClassType classType = null;
        for (Map.Entry<ClassType, List<Player>> mapEntry : classPlaces.entrySet()) {
            final int amountOfOverclass = mapEntry.getValue().size() - vacantClassNumber.get(mapEntry.getKey());
            if (amountOfOverclass > difference) {
                difference = amountOfOverclass;
                classType = mapEntry.getKey();
            }
        }
        return classType;
    }

    private ClassType findMostVacantClass() {
        int difference = 0;
        ClassType classType = null;
        for (Map.Entry<ClassType, List<Player>> mapEntry : classPlaces.entrySet()) {
            final int vacanciesLeft = vacantClassNumber.get(mapEntry.getKey()) - mapEntry.getValue().size();
            if (vacanciesLeft > difference) {
                difference = vacanciesLeft;
                classType = mapEntry.getKey();
            }
        }
        return classType;
    }

    private boolean isSwitchingNeeded() {
        //while at least one overclass and one vacant place
        final boolean atLeastOneOverclass = vacantClassNumber.entrySet()
                .stream()
                .map(classTypeIntegerEntry -> classPlaces.get(classTypeIntegerEntry.getKey()).size() > classTypeIntegerEntry.getValue())
                .reduce(false, Boolean::logicalOr);

        final boolean atLeastOneVaccant = vacantClassNumber.entrySet()
                .stream()
                .map(classTypeIntegerEntry -> classPlaces.get(classTypeIntegerEntry.getKey()).size() < classTypeIntegerEntry.getValue())
                .reduce(false, Boolean::logicalOr);

        LOGGER.info("Overclass founded: {}, vacancy founded: {}", atLeastOneOverclass, atLeastOneVaccant);
        return atLeastOneOverclass && atLeastOneVaccant;
    }

    private void fillFlexOnetrick(final List<Player> playerList) {
        playerList.forEach(player -> {
            if (player.getAvailableClasses().size() > 1) {
                flexPlayerList.add(player);
            } else {
                oneTrickPlayerList.add(player);
            }
        });
    }

    private void fillVacantNumber(final List<Player> captainList) {
        final Map<ClassType, Integer> occupiedSlots = new HashMap<>();
        occupiedSlots.put(ClassType.TANK, 0);
        occupiedSlots.put(ClassType.DPS, 0);
        occupiedSlots.put(ClassType.SUPPORT, 0);

        captainList
                .forEach(player -> occupiedSlots.computeIfPresent(player.getCurrentClass(), (classType, integer) -> integer + 1));
        occupiedSlots.forEach((key, value) -> vacantClassNumber.put(key, overallSlotsOfOneClass - value));
    }
}

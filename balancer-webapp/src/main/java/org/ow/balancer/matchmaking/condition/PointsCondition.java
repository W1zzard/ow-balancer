package org.ow.balancer.matchmaking.condition;

import org.ow.balancer.model.Player;

/**
 * Interface for additional custom rules which affect points of winning when swap calculated, which
 * could alter balancing result.
 */
public interface PointsCondition {
    Double apply(Player playerInTeam, Player playerCandidate);
}

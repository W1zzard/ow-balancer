package org.ow.balancer.dto.json2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * StatsDTO.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatsDto {
    private Map<String, ClassDto> classes;
}

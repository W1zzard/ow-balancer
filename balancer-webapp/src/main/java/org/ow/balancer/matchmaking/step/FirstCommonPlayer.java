package org.ow.balancer.matchmaking.step;

import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Add first common player by specific rules described in spec.
 */
public class FirstCommonPlayer implements MatchmakeStep {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirstCommonPlayer.class);

    private final List<Team> vacantTeamList = new ArrayList<>();
    private final List<Player> usedPlayerList = new ArrayList<>();

    @Override
    public List<Player> fillTeams(final List<Team> teamList, final List<Player> playerList) {
        vacantTeamList.addAll(teamList);

        playerList.sort(Comparator.comparingInt(Player::getCurrentClassMmr).reversed());
        vacantTeamList.sort(Comparator.comparingInt(o -> o.getCaptain().getCurrentClassMmr()));

        for (final Player playerCandidate : playerList) {
            Team firstBestTeam = null;
            Team secondBestTeam = null;

            for (final Team team : vacantTeamList) {
                if (team.isFreeSlot(playerCandidate.getCurrentClass())) {
                    if (secondBestTeam == null) {
                        secondBestTeam = team;
                    }
                    if (team.getCaptain().getCurrentClass() != playerCandidate.getCurrentClass()) {
                        firstBestTeam = team;
                        break;
                    }
                }
            }

            if (firstBestTeam != null) {
                insertPlayerToTeam(playerCandidate, firstBestTeam);
                // if not found best team then try second best
            } else if (secondBestTeam != null) {
                insertPlayerToTeam(playerCandidate, secondBestTeam);
                //if no second best try at least empty slot
            } else {
                LOGGER.error("Cant find even open slot for player: {}", playerCandidate);
            }

            //players could be more than team, finish step when player added to all teams
            if (vacantTeamList.isEmpty()) {
                break;
            }
        }
        return usedPlayerList;
    }

    private void insertPlayerToTeam(final Player playerCandidate, final Team foundedTeam) {
        foundedTeam.setThirdPlayer(playerCandidate);
        vacantTeamList.remove(foundedTeam);
        usedPlayerList.add(playerCandidate);
    }
}

package org.ow.balancer.configuration.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller for frontend forwarding.
 */
@Controller
public class FrontendForwardController {

    /**
     * Redirect all the paths that are not already mapped and do not contain dot.
     * Required to support HTML5 mode routing for single page app.
     */
    @RequestMapping("/**/{[path:[^.]*}")
    public String app(final HttpServletRequest request) {
        request.setAttribute(WebConfiguration.FRONTEND_REDIRECT, true);
        return "forward:/";
    }
}

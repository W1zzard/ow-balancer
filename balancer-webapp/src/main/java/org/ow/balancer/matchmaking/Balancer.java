package org.ow.balancer.matchmaking;

import org.ow.balancer.exception.UnableToProceedBalancingException;
import org.ow.balancer.matchmaking.condition.PointsCondition;
import org.ow.balancer.matchmaking.step.*;
import org.ow.balancer.model.ClassType;
import org.ow.balancer.model.Player;
import org.ow.balancer.model.PlayerType;
import org.ow.balancer.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Service to make balancing work.
 */
//CHECKSTYLE.OFF: ClassDataAbstractionCoupling
public class Balancer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Balancer.class);
    private static final int TEAM_SIZE_NO_CAP = 5;

    private final List<Player> commonPlayerList = new ArrayList<>();
    private final List<Player> captainList = new ArrayList<>();
    private final List<Player> weaponMasterList = new ArrayList<>();

    private final List<Team> teamList = new ArrayList<>();
    private final List<PointsCondition> pointsConditionList = new ArrayList<>();


    public Balancer(final List<Player> allPlayerList, final List<PointsCondition> pointsConditionList) {
        this.pointsConditionList.addAll(pointsConditionList);
        for (final Player player : allPlayerList) {
            switch (player.getPlayerType()) {
                case COMMON:
                    commonPlayerList.add(player);
                    break;
                case CAPTAIN:
                    captainList.add(player);
                    break;
                case WEAPONMASTER:
                    weaponMasterList.add(player);
                    break;
                default:
                    LOGGER.warn("Player cant be assigned to class: {}", player);
            }
        }
        validateListSize();
        //create teams by captain
        captainList.forEach(player -> {
            teamList.add(new Team(player));
        });
    }

    private void validateListSize() {
        if (captainList.size() * TEAM_SIZE_NO_CAP > commonPlayerList.size() + weaponMasterList.size()) {
            throw new UnableToProceedBalancingException("Not enough players for all captain teams");
        }
    }

    private void normalizeWeaponMasters() {
        if (weaponMasterList.size() > captainList.size()) {
            weaponMasterList.sort(Comparator.comparingInt(Player::getCurrentClassMmr).reversed());
            final int numberOfPLayersToCut = weaponMasterList.size() - captainList.size();
            for (int i = 0; i < numberOfPLayersToCut; i++) {
                final Player player = weaponMasterList.get(i);
                player.setPlayerType(PlayerType.COMMON);
                commonPlayerList.add(player);
            }
            weaponMasterList.removeAll(commonPlayerList);

        }
        //according to discuss with Anak lowest rang common player could be added  to weaponMasters.
        if (weaponMasterList.size() < captainList.size()) {
            commonPlayerList.sort(Comparator.comparingInt(Player::getCurrentClassMmr));
            final int playersToAdd = captainList.size() - weaponMasterList.size();
            for (int i = 0; i < playersToAdd; i++) {
                final Player player = commonPlayerList.get(i);
                player.setPlayerType(PlayerType.WEAPONMASTER);
                weaponMasterList.add(player);
            }
            commonPlayerList.removeAll(weaponMasterList);
        }
    }

    public List<Team> match() {
        normalizeWeaponMasters();
        rebalancePlayerClassQuantities();


        final MatchmakeStep stepWm = new MatchWeaponMasters();
        final FirstCommonPlayer stepFirstCommonPlayer = new FirstCommonPlayer();
        final SecondCommonPlayer stepSecondCommonPlayer = new SecondCommonPlayer();
        final FillFullTeam fillFullTeam = new FillFullTeam();

        stepWm.fillTeams(teamList, weaponMasterList);
        List<Player> usedPlayers = stepFirstCommonPlayer.fillTeams(teamList, commonPlayerList);
        commonPlayerList.removeAll(usedPlayers);
        usedPlayers = stepSecondCommonPlayer.fillTeams(teamList, commonPlayerList);
        commonPlayerList.removeAll(usedPlayers);
        usedPlayers = fillFullTeam.fillTeams(teamList, commonPlayerList);
        commonPlayerList.removeAll(usedPlayers);

        List<Player> notInTeamPlayerList = commonPlayerList;
        final BalanceWithExternalPlayersMainClass balanceWithExternalPlayersMainClass = new BalanceWithExternalPlayersMainClass();
        pointsConditionList.forEach(balanceWithExternalPlayersMainClass::addPointsCondition);
        notInTeamPlayerList = balanceWithExternalPlayersMainClass.fillTeams(teamList, notInTeamPlayerList);


        return teamList;
    }

    /**
     * If we have some class less then needed and another more than needed try to switch player to equalize class amount,
     * if we have a lot of different classes and each class more than team needed this method do nothing.
     */
    private void rebalancePlayerClassQuantities() {
        final ArrayList<Player> playerListToClassBalance = new ArrayList<>(weaponMasterList);
        playerListToClassBalance.addAll(commonPlayerList);

        Map<ClassType, Long> classNumber = playerListToClassBalance.stream().
                collect(Collectors.groupingBy(Player::getCurrentClass, Collectors.counting()));
        LOGGER.info("Class distribution before: {}", classNumber);
        final ClassesRebalance classesRebalance = new ClassesRebalance(captainList, playerListToClassBalance);
        classesRebalance.balanceClasses();
        classNumber = playerListToClassBalance.stream().
                collect(Collectors.groupingBy(Player::getCurrentClass, Collectors.counting()));
        LOGGER.info("Class distribution AFTER: {}", classNumber);
    }


}
//CHECKSTYLE.ON: ClassDataAbstractionCoupling

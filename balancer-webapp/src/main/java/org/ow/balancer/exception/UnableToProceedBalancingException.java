package org.ow.balancer.exception;

/**
 * Exception if matching/balancing cannot proceed further due unexpected situation.
 */
public class UnableToProceedBalancingException extends RuntimeException {
    public UnableToProceedBalancingException() {
    }

    public UnableToProceedBalancingException(final String message) {
        super(message);
    }

    public UnableToProceedBalancingException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnableToProceedBalancingException(final Throwable cause) {
        super(cause);
    }

    public UnableToProceedBalancingException(final String message, final Throwable cause, final boolean enableSuppression,
                                             final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

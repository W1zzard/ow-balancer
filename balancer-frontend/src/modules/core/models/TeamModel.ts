import {PlayerModel} from "modules/core/models/PlayerModel";


export interface TeamModel {
    name: string;
    averageMmr : number;
    playerList: PlayerModel[];
}

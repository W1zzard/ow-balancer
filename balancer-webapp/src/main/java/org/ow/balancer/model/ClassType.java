package org.ow.balancer.model;

/**
 * Enum with class type of player.
 */
public enum  ClassType {
    TANK(4), DPS(2), SUPPORT(0);

    private int maskShift;

    ClassType(final int maskShift) {
        this.maskShift = maskShift;
    }

    public int getMaskShift() {
        return maskShift;
    }
}

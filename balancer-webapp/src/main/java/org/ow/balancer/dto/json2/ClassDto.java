package org.ow.balancer.dto.json2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * Class dto.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClassDto {
    private int rank;
}

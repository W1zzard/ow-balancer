package org.ow.balancer.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Team model.
 */
public class Team {
    private static final int CAPTAIN_INDEX = 0;
    private static final int WEAPONMASTER_INDEX = 1;
    private static final int THIRD_PLAYER_INDEX = 2;
    private static final int FOURTH_PLAYER_INDEX = 3;
    private static final int COMMON_PLAYER_FROM_INDEX = 4;
    private static final int COMMON_PLAYER_TO_INDEX = 6;

    private final TeamClassSlot teamClassSlot = new TeamClassSlot();
    private final List<Player> playerList = new ArrayList<>(6);

    private String name;
    private int mmrSum;


    public Team() {
    }

    public Team(final Player player) {
        this.name = player.getUniqueName();
        insertPlayer(player, CAPTAIN_INDEX);
    }

    public String getName() {
        return name;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public boolean isFreeSlot(final ClassType classType) {
        return teamClassSlot.isFreeSlot(classType);
    }

    @JsonIgnore
    public int getMmrSum() {
        return mmrSum;
    }

    public double getAverageMmr() {
        return (double) getMmrSum() / playerList.size();
    }

    @JsonIgnore
    public Player getCaptain() {
        return playerList.get(CAPTAIN_INDEX);
    }

    public void setWeaponMaster(final Player player) {
        insertPlayer(player, WEAPONMASTER_INDEX);
    }

    @JsonIgnore
    public Player getWeaponMaster() {
        return playerList.get(WEAPONMASTER_INDEX);
    }

    public void setThirdPlayer(final Player player) {
        insertPlayer(player, THIRD_PLAYER_INDEX);
    }

    @JsonIgnore
    public Player getThirdPlayer() {
        return playerList.get(THIRD_PLAYER_INDEX);
    }

    public void setFourthPlayer(final Player player) {
        insertPlayer(player, FOURTH_PLAYER_INDEX);
    }

    @JsonIgnore
    public Player getFourthPlayer() {
        return playerList.get(FOURTH_PLAYER_INDEX);
    }

    @JsonIgnore
    public List<Player> getSwapablePlayers() {
        return playerList.subList(COMMON_PLAYER_FROM_INDEX, COMMON_PLAYER_TO_INDEX);
    }

    public void insertPlayer(final Player player) {
        playerList.add(player);
        player.setTeam(this);
        updateMmr();
        teamClassSlot.occupySlot(player.getCurrentClass());
    }

    public void removePlayer(final Player player) {
        playerList.remove(player);
        player.setTeam(null);
        teamClassSlot.freeSlot(player.getCurrentClass());
        updateMmr();
    }

    private void insertPlayer(final Player player, final int index) {
        playerList.add(index, player);
        player.setTeam(this);
        updateMmr();
        teamClassSlot.occupySlot(player.getCurrentClass());
    }

    public void replacePlayerByPlayer(final Player playerInTeam, final Player newPlayer) {
        playerList.remove(playerInTeam);
        playerInTeam.setTeam(null);
        teamClassSlot.freeSlot(playerInTeam.getCurrentClass());
        insertPlayer(newPlayer);
    }


    private void updateMmr() {
        mmrSum = playerList.stream()
                .filter(Objects::nonNull)
                .mapToInt(Player::getCurrentClassMmr)
                .sum();
    }
}

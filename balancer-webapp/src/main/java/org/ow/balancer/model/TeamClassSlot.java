package org.ow.balancer.model;

/**
 * structure to store classes like bits, 00 tanks 00 dps 00 support.
 */
//CHECKSTYLE.OFF: MagicNumber
public class TeamClassSlot {
    // 000000
    private int classSlots;

    public boolean isFreeSlot(final ClassType classType) {
        final int currentClassMask = classSlots >> classType.getMaskShift() & 3;
        // 11 is 3 means all occupy
        return currentClassMask < 3;
    }

    public void occupySlot(final ClassType classType) {
        int currentClassMask = classSlots >> classType.getMaskShift() & 3;
        //set bit to 1 to switch it to 1 with OR
        if (currentClassMask == 0) {
            currentClassMask = 1;
        } else if (currentClassMask == 1) {
            currentClassMask = 3;
        } else {
            throw new IllegalArgumentException("All slots are occupied");
        }
        classSlots = classSlots | currentClassMask << classType.getMaskShift();
    }

    public void freeSlot(final ClassType classType) {
        int currentClassMask = classSlots >> classType.getMaskShift() & 3;
        //set bit to 1 to switch it to 0 with xor
        if (currentClassMask == 3) {
            currentClassMask = 2;
        } else {
            currentClassMask = 1;
        }
        classSlots = classSlots ^ currentClassMask << classType.getMaskShift();
    }
}
//CHECKSTYLE.ON: MagicNumber

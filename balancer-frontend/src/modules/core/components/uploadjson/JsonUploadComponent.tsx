import React, {useState} from 'react';
import { Button } from 'react-bootstrap';
import './styles.css'

const JsonUploadComponent: React.FC = () => {
    const [jsonString, setJsonString] = useState('');
    const [saved, setSaved] = useState(0);
    const [clearingStatus, setClearingStatus] = useState('false');

    function handleChange(event: any) {
        setJsonString(event.target.value);
    }

    function handleSend() {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: jsonString
        };
        fetch('/api/v1/player/batchLoad', requestOptions)
            .then(response => response.json())
            .then(data => setSaved(data));
    }

    function clearPlayers() {
        setClearingStatus('false');
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'}
        };
        fetch('/api/v1/admin/reset', requestOptions)
            .then(response => response.text())
            .then(data => setClearingStatus(data));
    }

    return (
        <div>
            <div className="row">
                <div className="col-lg-12">
                    <textarea className="json_textarea" name="content" value={jsonString} onChange={handleChange}/>
                </div>
            </div>
            <div className="row mt-1">
                <div className="col-lg-1">
                    <Button onClick={handleSend}>Send</Button>
                </div>
                <div className="col-lg-1">
                    {saved}
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-lg-1">
                    <Button onClick={clearPlayers}>Clear</Button>
                </div>
                <div className="col-lg-1">
                    {clearingStatus}
                </div>
            </div>
        </div>
    );
};

export default JsonUploadComponent;

import React from 'react';
import JsonUploadComponent from "./modules/core/components/uploadjson/JsonUploadComponent";
import BalancerComponent from "./modules/core/components/balancer/BalancerComponent";


const AppComponent: React.FC = () => {
  return (
      <div className="app-container">
        <div className="container">
          <div className="row">
            <JsonUploadComponent/>
          </div>
          <div className="row">
            <BalancerComponent/>
          </div>
        </div>
      </div>
  );
};

export default AppComponent;

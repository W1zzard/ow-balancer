package org.ow.balancer.matchmaking.step;

import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;

import java.util.List;

/**
 * Interface for steps of matchmaking.
 */
public interface MatchmakeStep {

    /**
     * Fill team with players from playerList according to some rules specific to step and return list of used player.
     *
     * @param teamList list of teams to fill
     * @param playerList list of available players
     * @return list of players added to list
     */
    List<Player> fillTeams(List<Team> teamList, List<Player> playerList);
}

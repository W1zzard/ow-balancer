package org.ow.balancer.configuration;

import org.springframework.web.util.UriComponentsBuilder;
import springfox.documentation.spring.web.paths.AbstractPathProvider;
import springfox.documentation.spring.web.paths.Paths;

/**
 * Path provider to override swagger base path.
 */
public class BasePathAwareRelativePathProvider extends AbstractPathProvider {

    private static final String SLASH = "/";

    private String basePath;

    public BasePathAwareRelativePathProvider(final String basePath) {
        this.basePath = basePath;
    }

    @Override
    protected String applicationPath() {
        return basePath;
    }

    @Override
    protected String getDocumentationPath() {
        return SLASH;
    }

    @Override
    public String getOperationPath(final String operationPath) {
        final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromPath(SLASH);
        return Paths.removeAdjacentForwardSlashes(
                uriComponentsBuilder.path(operationPath.replaceFirst(basePath, "")).build().toString());
    }
}

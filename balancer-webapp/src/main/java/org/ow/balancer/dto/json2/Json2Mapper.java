package org.ow.balancer.dto.json2;

import org.ow.balancer.exception.UnableToProceedBalancingException;
import org.ow.balancer.model.ClassSkill;
import org.ow.balancer.model.ClassType;
import org.ow.balancer.model.Player;
import org.ow.balancer.model.PlayerType;
import org.springframework.stereotype.Component;

import java.util.Comparator;

/**
 * mapper for 2nd json type.
 */
@Component
public class Json2Mapper {

    public Player toPlayer(final PlayerDto source) {
        final Player player = new Player();
        player.setUniqueName(source.getIdentity().getName());
        player.setDisplayName(source.getIdentity().getName());
        fillMainClassMmr(source, player);
        fillPlayerType(source, player);
        return player;
    }

    private void fillPlayerType(final PlayerDto playerDto, final Player player) {
        if (playerDto.getIdentity().getIsCaptain()) {
            player.setPlayerType(PlayerType.CAPTAIN);
        } else if (playerDto.getIdentity().getIsSquire()) {
            player.setPlayerType(PlayerType.WEAPONMASTER);
        } else {
            player.setPlayerType(PlayerType.COMMON);
        }
    }

    private void fillMainClassMmr(final PlayerDto playerDto, final Player player) {
        final int tankRank = playerDto.getStats().getClasses().get("tank").getRank();
        if (tankRank > 0) {
            player.getAvailableClasses().add(new ClassSkill(ClassType.TANK, tankRank));
        }
        final int dpsRank = playerDto.getStats().getClasses().get("dps").getRank();
        if (dpsRank > 0) {
            player.getAvailableClasses().add(new ClassSkill(ClassType.DPS, dpsRank));
        }
        final int supportRank = playerDto.getStats().getClasses().get("support").getRank();
        if (supportRank > 0) {
            player.getAvailableClasses().add(new ClassSkill(ClassType.SUPPORT, supportRank));
        }

        if (player.getAvailableClasses().isEmpty()) {
            throw new UnableToProceedBalancingException("No class for player: " + player.toString());
        }

        player.getAvailableClasses().sort(Comparator.comparingInt(ClassSkill::getMmr).reversed());
        int priority = 1;
        for (ClassSkill classSkill : player.getAvailableClasses()) {
            classSkill.setPriority(priority);
            priority++;
        }
        player.setCurrentClass(player.getAvailableClasses().get(0).getClassType());
        player.setCurrentClassMmr(player.getAvailableClasses().get(0).getMmr());
        player.setCurrentClassSkill(player.getAvailableClasses().get(0));
    }
}

package org.ow.balancer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow.balancer.matchmaking.Balancer;
import org.ow.balancer.matchmaking.condition.PointsCondition;
import org.ow.balancer.matchmaking.condition.TwoLowClassCondition;
import org.ow.balancer.model.BalanceRequest;
import org.ow.balancer.model.Player;
import org.ow.balancer.model.Team;
import org.ow.balancer.repository.Storage;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service to balance players.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class BalanceService {
    private final Storage storage;

    public List<Team> getBalancedTeam(final BalanceRequest balanceRequest) {
        final double tankWeightCoeff = balanceRequest.getTankMmrWeight() / 100d;
        final double dpsWeightCoeff = balanceRequest.getDpsMmrWeight() / 100d;
        final double supportWeightCoeff = balanceRequest.getSupportMmrWeight() / 100d;
        final List<Player> allPlayers = storage.getAllPlayers();
        allPlayers.forEach(player -> {
            player.getAvailableClasses().forEach(classSkill -> {
                switch (classSkill.getClassType()) {
                    case TANK:
                        classSkill.setMmr((int) (classSkill.getMmr() * tankWeightCoeff));
                        break;
                    case DPS:
                        classSkill.setMmr((int) (classSkill.getMmr() * dpsWeightCoeff));
                        break;
                    case SUPPORT:
                        classSkill.setMmr((int) (classSkill.getMmr() * supportWeightCoeff));
                        break;
                    default:
                }
            });
            //update changed value in current
            player.switchClassTo(player.getCurrentClass());
        });

        final List<PointsCondition> pointsConditionList = new ArrayList<>();
        if (balanceRequest.getLowSkillMargin() > 0) {
            log.info("Create Low class condition");
            final TwoLowClassCondition condition = new TwoLowClassCondition((double) balanceRequest.getLowSkillMargin(),
                    (double) balanceRequest.getLowSkillSameClassPenalty());
            pointsConditionList.add(condition);
        }

        final Balancer balancer = new Balancer(allPlayers, pointsConditionList);
        return balancer.match();
    }
}

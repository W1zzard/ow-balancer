package org.ow.balancer.dto.json1;

import org.ow.balancer.exception.UnableToProceedBalancingException;
import org.ow.balancer.model.ClassSkill;
import org.ow.balancer.model.ClassType;
import org.ow.balancer.model.Player;
import org.ow.balancer.model.PlayerType;
import org.springframework.stereotype.Component;

import java.util.Comparator;

/**
 * mapper for 1st json type.
 */
@Component
public class Json1Mapper {
    public Player toPlayer(final PlayerDto source) {
        final Player player = new Player();
        player.setUniqueName(source.getId());
        player.setDisplayName(source.getDisplayName());
        fillMainClassMmr(source, player);
        fillPlayerType(source, player);
        return player;
    }

    private void fillPlayerType(final PlayerDto playerDto, final Player player) {
        if (playerDto.isCaptain()) {
            player.setPlayerType(PlayerType.CAPTAIN);
        } else {
            player.setPlayerType(PlayerType.COMMON);
        }
    }

    private void fillMainClassMmr(final PlayerDto playerDto, final Player player) {
        if (playerDto.getSrByClass().getTank() > 0) {
            player.getAvailableClasses().add(new ClassSkill(ClassType.TANK, playerDto.getSrByClass().getTank()));
        }
        if (playerDto.getSrByClass().getDps() > 0) {
            player.getAvailableClasses().add(new ClassSkill(ClassType.DPS, playerDto.getSrByClass().getDps()));
        }
        if (playerDto.getSrByClass().getSupport() > 0) {
            player.getAvailableClasses().add(new ClassSkill(ClassType.SUPPORT, playerDto.getSrByClass().getSupport()));
        }

        if (player.getAvailableClasses().isEmpty()) {
            throw new UnableToProceedBalancingException("No class for player: " + player.toString());
        }

        player.getAvailableClasses().sort(Comparator.comparingInt(ClassSkill::getMmr).reversed());
        int priority = 1;
        for (ClassSkill classSkill : player.getAvailableClasses()) {
            classSkill.setPriority(priority);
            priority++;
        }
        player.setCurrentClass(player.getAvailableClasses().get(0).getClassType());
        player.setCurrentClassMmr(player.getAvailableClasses().get(0).getMmr());
        player.setCurrentClassSkill(player.getAvailableClasses().get(0));
    }
}

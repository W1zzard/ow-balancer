package org.ow.balancer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.ow.balancer.configuration.RestConfigurationUtils;
import org.ow.balancer.configuration.SwaggerTags;
import org.ow.balancer.repository.Storage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for global administration request like whole wipe.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(RestConfigurationUtils.BASE_PATH_V1 + "/admin")
@Api(tags = SwaggerTags.ADMIN)
public class AdminController {
    private final Storage storage;

    @ApiOperation(value = "Reset all saved data", notes = "Used to remove everything and start again")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @PostMapping("/reset")
    public boolean reset() {
        storage.clear();
        return true;
    }
}

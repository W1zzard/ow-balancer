export interface PlayerModel {
    uniqueName: string;
    displayName: string;
    playerType: string;
    currentClassSkill: {
        classType: string;
        mmr: number;
        priority: number;
    };
}

package org.ow.balancer.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static org.ow.balancer.configuration.RestConfigurationUtils.getApiPaths;
import static org.ow.balancer.configuration.RestConfigurationUtils.tag;

/**
 * Content REST configuration.
 */
@Configuration
@EnableSwagger2
public class RestConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("balancer")
                .tags(
                        tag(SwaggerTags.ADMIN),
                        tag(SwaggerTags.PLAYERS)
                )
                .apiInfo(apiInfo())
                .pathProvider(new BasePathAwareRelativePathProvider(RestConfigurationUtils.BASE_PATH_V1))
                .select()
                .paths(getApiPaths(RestConfigurationUtils.BASE_PATH_V1))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Balancer REST API")
                .description("Balancer for OW tournaments")
                .build();
    }

    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                // https://github.com/springfox/springfox/issues/2201
                .validatorUrl("")
                .build();
    }
}

package org.ow.balancer.model;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class TeamClassSlotTest extends TestCase {


    @Test
    public void testTeamClassSlotMask() {
        final TeamClassSlot teamClassSlot = new TeamClassSlot();

        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.TANK));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.DPS));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.SUPPORT));

        //set 1 slot all class
        teamClassSlot.occupySlot(ClassType.TANK);
        teamClassSlot.occupySlot(ClassType.DPS);
        teamClassSlot.occupySlot(ClassType.SUPPORT);

        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.TANK));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.DPS));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.SUPPORT));

        //set 2 slot all class
        teamClassSlot.occupySlot(ClassType.TANK);
        teamClassSlot.occupySlot(ClassType.DPS);
        teamClassSlot.occupySlot(ClassType.SUPPORT);

        Assert.assertFalse(teamClassSlot.isFreeSlot(ClassType.TANK));
        Assert.assertFalse(teamClassSlot.isFreeSlot(ClassType.DPS));
        Assert.assertFalse(teamClassSlot.isFreeSlot(ClassType.SUPPORT));

        //free 1 slot
        teamClassSlot.freeSlot(ClassType.TANK);
        teamClassSlot.freeSlot(ClassType.DPS);
        teamClassSlot.freeSlot(ClassType.SUPPORT);

        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.TANK));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.DPS));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.SUPPORT));

        //free 2nd slot
        teamClassSlot.freeSlot(ClassType.TANK);
        teamClassSlot.freeSlot(ClassType.DPS);
        teamClassSlot.freeSlot(ClassType.SUPPORT);

        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.TANK));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.DPS));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.SUPPORT));

        //set 1 slot all class
        teamClassSlot.occupySlot(ClassType.TANK);
        teamClassSlot.occupySlot(ClassType.DPS);
        teamClassSlot.occupySlot(ClassType.SUPPORT);

        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.TANK));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.DPS));
        Assert.assertTrue(teamClassSlot.isFreeSlot(ClassType.SUPPORT));
    }

    @Test
    public void testExceptionTank() {
        final TeamClassSlot teamClassSlot = new TeamClassSlot();
        teamClassSlot.occupySlot(ClassType.TANK);
        teamClassSlot.occupySlot(ClassType.TANK);
        try {
            teamClassSlot.occupySlot(ClassType.TANK);
            Assert.fail();
        } catch (IllegalArgumentException exception) {
            Assert.assertNotNull(exception);
        }
    }

    @Test
    public void testExceptionDps() {
        final TeamClassSlot teamClassSlot = new TeamClassSlot();
        teamClassSlot.occupySlot(ClassType.DPS);
        teamClassSlot.occupySlot(ClassType.DPS);
        try {
            teamClassSlot.occupySlot(ClassType.DPS);
            Assert.fail();
        } catch (IllegalArgumentException exception) {
            Assert.assertNotNull(exception);
        }
    }

    @Test
    public void testExceptionSupport() {
        final TeamClassSlot teamClassSlot = new TeamClassSlot();
        teamClassSlot.occupySlot(ClassType.SUPPORT);
        teamClassSlot.occupySlot(ClassType.SUPPORT);
        try {
            teamClassSlot.occupySlot(ClassType.SUPPORT);
            Assert.fail();
        } catch (IllegalArgumentException exception) {
            Assert.assertNotNull(exception);
        }
    }

}
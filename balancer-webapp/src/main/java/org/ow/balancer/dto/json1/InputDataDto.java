package org.ow.balancer.dto.json1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * DTO for parsing input json.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class InputDataDto {
    @JsonProperty("format_type")
    private String formatType;
    private List<PlayerDto> players;
}
